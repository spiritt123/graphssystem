
QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets


TARGET = bin/saper.exe
TEMPLATE = app

#VPATH = source
INCLUDEPATH = include

SOURCES += src/*.cpp

HEADERS += include/*.h

FORMS    += mainwindow.ui

