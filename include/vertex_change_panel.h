#pragma once

#include <QWidget>
#include <QString>
#include <QVBoxLayout>
#include "graph_storage_system.h"
#include "vertex_change_subpanel.h"
#include <string>

class VertexChangePanel : public QWidget
{
    Q_OBJECT
public:
    VertexChangePanel(QWidget *parent, GraphStorageSystem *graph_storage_system);
    ~VertexChangePanel();

// bool (*callback)()
private:
    bool addVertex(std::string name);
    bool removeVertex(std::string name);

private:
    QVBoxLayout *_vertical_list_panels;
    GraphStorageSystem *_graph_storage_system;

};


