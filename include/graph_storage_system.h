#pragma once

#include <QDebug>
#include <iostream>
#include <string>
#include <vector>
#include <map>

class Vertex
{
public:
    Vertex(std::string name) : _name(name) {}
    ~Vertex() {}
    std::string getName() const { return _name; };
    friend bool operator==(const Vertex& left, const Vertex& right);

private:
    std::string _name;
};

class Edge
{
public:
    Edge(std::string start_id, std::string end_id) : _start_id(start_id), _end_id(end_id) {std::cout << _start_id << "\n";}
    ~Edge() {}
    std::pair<std::string, std::string> getEdge() { return std::make_pair(_start_id, _end_id); };

private:
    std::string _start_id;
    std::string _end_id;
};

class GraphStorageSystem
{
private:
    std::vector<Vertex> _vertexes;
    std::vector<Edge> _edges;


public:
    GraphStorageSystem(std::string file_name) 
    {
    }
    ~GraphStorageSystem() {}

    bool addNewVertex(std::string vertex_name) 
    {
        _vertexes.push_back(Vertex(vertex_name));
        return true;
    }

    bool addNewEdge(std::string start, std::string end) 
    {
        //auto itr_start   = std::find(_vertexes.begin(), _vertexes.end(), _maps[start]);
        //auto itr_end     = std::find(_vertexes.begin(), _vertexes.end(), _maps[end]);
        _edges.push_back(Edge(start, end));
        return true;
    } 

    bool removeCurrentVertex(std::string vertex) {return true;} //;
    bool removeCurrentEdge(std::string start, std::string end) {return true;} //;

    std::vector<Vertex> getVertexes()
    {
        return _vertexes;
    } 

    std::vector<Edge> getEdges() 
    {
        return _edges;
    }


    int getVectexesCount() { return _vertexes.size(); }

//    Vertex getVertexByName(std::string name) {return {"a";};} //;
//    Vertex getVertexByIndex(int index) {return {"a";};} //;

    void saveState() {};
};

