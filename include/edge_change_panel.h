
#pragma once

#include <QWidget>
#include <QString>
#include <QVBoxLayout>
#include "graph_storage_system.h"
#include "edge_change_subpanel.h"
#include <string>

class EdgeChangePanel : public QWidget
{
    Q_OBJECT
public:
    EdgeChangePanel(QWidget *parent, GraphStorageSystem *graph_storage_system);
    ~EdgeChangePanel();

private:
    bool addEdge(std::string start, std::string end);
    bool removeEdge(std::string start, std::string end);

private:
    QVBoxLayout *_vertical_list_panels;
    GraphStorageSystem *_graph_storage_system;

};

