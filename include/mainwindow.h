#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTimer>
#include <QTime>
#include <QPaintEvent>
#include <QPainter>
#include <QPoint>

#include "vertex_change_panel.h"
#include "edge_change_panel.h"
#include "table_change_panel.h"

#include "graph_storage_system.h"


namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void renderGraph();

private:
    void drawVertex(QPointF center_point, QPointF current_point);
    void drawEdge(QPointF center_point, QPointF current_point);

    void hideAllWidget();

private:
    Ui::MainWindow *ui;
    QPainter *_painter;

    VertexChangePanel *_vertex_panel;
    EdgeChangePanel   *_edge_panel;
    TableChangePanel  *_table_panel;
    
    GraphStorageSystem *_graph_storage_system;

protected:
    void paintEvent(QPaintEvent *event);

public slots:
    void openProject();
    void createProject();
    void saveProject();
    void viewGraph();

    void openVertexPanel();
    void openEdgePanel();
    void openTablePanel();
};

#endif // MAINWINDOW_H

