#pragma once

#include <QString>
#include <QWidget>
#include <QHBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QDebug>
#include <string>

typedef std::function<bool(std::string, std::string)> Callback2;

class EdgeChangeSubpanel : public QWidget
{
    Q_OBJECT
public:
    EdgeChangeSubpanel(QString name_action, QWidget *parent, Callback2 call);
    ~EdgeChangeSubpanel();

private:
    Callback2 _call;
    
public slots:
    void modifyAction();

private:
    QHBoxLayout *_horisontal_list_panel;
    QLabel *_label;
    QString _name_action;
    QLineEdit *_start;
    QLineEdit *_end;
    QPushButton *_button;
};
