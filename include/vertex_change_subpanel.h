#pragma once

#include <QString>
#include <QWidget>
#include <QHBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QDebug>
#include <string>

typedef std::function<bool(std::string)> Callback;

class VertexChangeSubpanel : public QWidget
{
    Q_OBJECT
public:
    VertexChangeSubpanel(QString name_action, QWidget *parent, Callback call);
    ~VertexChangeSubpanel();

private:
    Callback _call;
    
public slots:
    void modifyAction();

private:
    QHBoxLayout *_horisontal_list_panel;
    QLabel *_label;
    QString _name_action;
    QLineEdit *_line;
    QPushButton *_button;
};



