#pragma once

#include <QWidget>
#include <QTableWidget>
#include "graph_storage_system.h"

class TableChangePanel : public QWidget
{
    Q_OBJECT    
public:
    TableChangePanel(QWidget *parant, GraphStorageSystem *graph_storage_system);
    ~TableChangePanel();

    void update();

private:
    QTableWidget *_table;
    GraphStorageSystem *_graph_storage_system;
};
