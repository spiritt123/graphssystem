#include "graph_storage_system.h"

bool operator==(const Vertex& left, const Vertex& right)
{
    return left.getName() == right.getName();
}

