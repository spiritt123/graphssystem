
#include "edge_change_panel.h"


EdgeChangePanel::EdgeChangePanel(QWidget *parent, GraphStorageSystem *graph_storage_system)
    : QWidget(parent)
    , _graph_storage_system(graph_storage_system)
{
    _vertical_list_panels = new QVBoxLayout(this);
    
    
    _vertical_list_panels->addWidget(
        new EdgeChangeSubpanel(
            "add", this, std::bind(
                &EdgeChangePanel::addEdge, 
                this, 
                std::placeholders::_1,
                std::placeholders::_2
                )
            )
        );
    
    _vertical_list_panels->addWidget(
        new EdgeChangeSubpanel(
            "remove", this, std::bind(
                &EdgeChangePanel::removeEdge, 
                this, 
                std::placeholders::_1,
                std::placeholders::_2
                )
            )
        );
}

EdgeChangePanel::~EdgeChangePanel()
{}

bool EdgeChangePanel::addEdge(std::string start, std::string end)
{
    return _graph_storage_system->addNewEdge(start, end);
}

bool EdgeChangePanel::removeEdge(std::string start, std::string end)
{
    return _graph_storage_system->removeCurrentEdge(start, end);
}

