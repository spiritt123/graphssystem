#include "vertex_change_panel.h"


VertexChangePanel::VertexChangePanel(QWidget *parent, GraphStorageSystem *graph_storage_system)
    : QWidget(parent)
    , _graph_storage_system(graph_storage_system)
{
    _vertical_list_panels = new QVBoxLayout(this);
    _vertical_list_panels->addWidget(
        new VertexChangeSubpanel(
            "add", this, std::bind(
                &VertexChangePanel::addVertex, this, std::placeholders::_1)
            )
        );
    _vertical_list_panels->addWidget(
        new VertexChangeSubpanel(
            "remove", this, std::bind(
                &VertexChangePanel::removeVertex, this, std::placeholders::_1)
            )
        );
}

VertexChangePanel::~VertexChangePanel()
{}

bool VertexChangePanel::addVertex(std::string name)
{
    return _graph_storage_system->addNewVertex(name);
}

bool VertexChangePanel::removeVertex(std::string name)
{
    return _graph_storage_system->removeCurrentVertex(name);
}

