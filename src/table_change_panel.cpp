#include "table_change_panel.h"
#include <iostream>

TableChangePanel::TableChangePanel(QWidget *parant, GraphStorageSystem *graph_storage_system)
    : QWidget(parant)
    , _graph_storage_system(graph_storage_system)
{
    _table = new QTableWidget(this);
    _table->setSelectionMode(QAbstractItemView::ExtendedSelection);
}

TableChangePanel::~TableChangePanel()
{}


void TableChangePanel::update()
{
    auto vertexes = _graph_storage_system->getVertexes();
    auto edges    = _graph_storage_system->getEdges();

    int w = vertexes.size();
    int h = w;
    _table->setColumnCount(w);
    _table->setRowCount(h);
    resize(w * 500, h * 500);
    _table->resize(w * 100 + 30, h * 50);

    for (int i = 0; i < h; ++i)
    {
        _table->setHorizontalHeaderItem(i, new QTableWidgetItem( QString(vertexes[i].getName().c_str()) ));
        _table->setVerticalHeaderItem  (i, new QTableWidgetItem( QString(vertexes[i].getName().c_str()) ));
    }

    //for (const)

    _table->show();
}
