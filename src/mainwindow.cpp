#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QVBoxLayout>
#include <iostream>
#include <QPaintEvent>
#include <QPainter>
#include <QPoint>
#include <QDebug>
#include <cmath>
#include <QMenuBar>
#include <QMenu>
#include <QAction>
#include <QFileDialog>
#include <QFile>
#include <iostream>
#include "graph_storage_system.h"

#define PI 3.14159265358979323846264338327950288419716939937510

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    connect(ui->actionOpen,   SIGNAL(triggered()), this, SLOT(openProject()));
    connect(ui->actionCreate, SIGNAL(triggered()), this, SLOT(createProject()));
    connect(ui->actionSave,   SIGNAL(triggered()), this, SLOT(saveProject()));
    connect(ui->actionView,   SIGNAL(triggered()), this, SLOT(viewGraph()));

    connect(ui->actionVertex, SIGNAL(triggered()), this, SLOT(openVertexPanel()));
    connect(ui->actionEdge,   SIGNAL(triggered()), this, SLOT(openEdgePanel()));
    connect(ui->actionTable,  SIGNAL(triggered()), this, SLOT(openTablePanel()));

    _graph_storage_system = new GraphStorageSystem("new_window");
    QWidget *main_widget  = new QWidget();
    ui->verticalLayout->addWidget(main_widget);

    _vertex_panel = new VertexChangePanel(main_widget, _graph_storage_system);
    _edge_panel   = new EdgeChangePanel(main_widget, _graph_storage_system);
    _table_panel  = new TableChangePanel(main_widget, _graph_storage_system);

    hideAllWidget();
    main_widget->show();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::renderGraph()
{
    this->update();
}

static QPointF rotateVectorByAngle(QPointF vector, double angle)
{
    QPointF new_point = 
    {
        vector.x() * cos(angle) - vector.y() * sin(angle),
        vector.y() * cos(angle) + vector.x() * sin(angle)
    };
    return new_point;
}

static QPointF getPointByAngle(QPointF center_point, double R, double angle)
{
    return rotateVectorByAngle({R, 0}, angle) + center_point;
}

static double distance(QPointF vector)
{
    return std::sqrt(vector.x() * vector.x() + vector.y() * vector.y());
}

static double distance(QPointF start, QPointF end)
{
    return distance(end - start);
}

void MainWindow::paintEvent(QPaintEvent *event)
{
    _painter = new QPainter(this);

    double R = 200;
    QPointF current_point = QPointF(this->width() / 2 + R, this->height() / 2);
    QPointF center_point  = QPointF(this->width() / 2, this->height() / 2);
    
    drawEdge(center_point, current_point);
    drawVertex(center_point, current_point);

    delete _painter;
};

void MainWindow::drawVertex(QPointF center_point, QPointF current_point)
{
    auto vertex = _graph_storage_system->getVertexes();
    int count = vertex.size();
    double angle = 2 * PI / count;

    for (int i = 0; i < count; ++i)
    {
        QPointF buffer_point = current_point - center_point;
        QPointF new_point = rotateVectorByAngle(buffer_point, angle);
        current_point = new_point + center_point;

        _painter->setBrush(Qt::red);
        _painter->drawEllipse(current_point, 30, 30);
    }
}


void MainWindow::drawEdge(QPointF center_point, QPointF current_point)
{
    double R = 200;

    auto vertex = _graph_storage_system->getVertexes();
    auto edges  = _graph_storage_system->getEdges();

    double angle = 2 * PI / vertex.size();
    
    for (auto &edge : edges)
    {
        auto itr_start = std::find(vertex.begin(), vertex.end(), Vertex(edge.getEdge().first));
        auto itr_end   = std::find(vertex.begin(), vertex.end(), Vertex(edge.getEdge().second));

        if (itr_start == vertex.end() || itr_end == vertex.end())
        {
            continue;
        }

        QPointF start = getPointByAngle(center_point, R, angle * std::distance(vertex.begin(), itr_start));
        QPointF end   = getPointByAngle(center_point, R, angle * std::distance(vertex.begin(), itr_end));

        QPointF pm = (start + end) / 2;

        double new_R = 700;

        QPointF normal = rotateVectorByAngle(end - pm, -PI / 2) / distance(end, pm);
        QPointF new_center = QPointF(normal.x() * new_R, normal.y() * new_R) + center_point;

        new_R = distance(new_center, start);
        QPointF norm1 = (start - new_center) / new_R;
        QPointF norm2 = (end   - new_center) / new_R;
        
        double start_angle = 16 * ((atan2(-norm1.y(), norm1.x())  + 2 * PI) * 180 / PI);
        double span_angle  = 16 * ((atan2(-norm2.y(), norm2.x())  + 2 * PI) * 180 / PI) - start_angle;

        _painter->drawArc(
            new_center.x() - new_R,
            new_center.y() - new_R,
            new_R * 2,
            new_R * 2,
            start_angle,
            span_angle + 30
        );
    }
}

void MainWindow::hideAllWidget()
{
    _vertex_panel->hide();
    _edge_panel->hide();
    _table_panel->hide();
}

void MainWindow::viewGraph()
{
    hideAllWidget();
    renderGraph();
}

void MainWindow::openVertexPanel()
{
    hideAllWidget();
    _vertex_panel->show();
}
void MainWindow::openEdgePanel()
{
    hideAllWidget();
    _edge_panel->show();
}
void MainWindow::openTablePanel()
{
    hideAllWidget();
    _table_panel->show();
    _table_panel->update();
}

void MainWindow::openProject()
{
    if (_graph_storage_system != nullptr)
    {
        //unsave
        delete _graph_storage_system;
        _graph_storage_system = nullptr;
    }
    QString filename = QFileDialog::getOpenFileName(this, tr("Open File"), "./", "All files (*.graph)");

    _graph_storage_system = new GraphStorageSystem(filename.toStdString());
}

void MainWindow::createProject()
{
    QString filename = QFileDialog::getSaveFileName(0, tr("Open Directory"), "", tr("All files (*.graph)", ""));

    //std::cout << dir.toStdString() << "\n";
    if (_graph_storage_system != nullptr)
    {
        delete _graph_storage_system;
        _graph_storage_system = nullptr;
    }
    _graph_storage_system = new GraphStorageSystem(filename.toStdString());
}

void MainWindow::saveProject()
{
    _graph_storage_system->saveState();
}

