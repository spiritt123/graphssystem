#include "vertex_change_subpanel.h"


VertexChangeSubpanel::VertexChangeSubpanel(QString name_action, QWidget *parent, Callback call)
    : QWidget(parent)
    , _name_action(name_action)
    , _call(call)
{
    _horisontal_list_panel = new QHBoxLayout(this);

    _label = new QLabel(_name_action);
    _line = new QLineEdit();
    _button = new QPushButton("Push");

    connect(_button, SIGNAL(clicked()), this, SLOT(modifyAction()));

    _horisontal_list_panel->addWidget(_label);
    _horisontal_list_panel->addWidget(_line);
    _horisontal_list_panel->addWidget(_button);
}

VertexChangeSubpanel::~VertexChangeSubpanel()
{}

void VertexChangeSubpanel::modifyAction()
{
    qDebug() << _call(_line->text().toStdString());
}

