#include "edge_change_subpanel.h"

EdgeChangeSubpanel::EdgeChangeSubpanel(QString name_action, QWidget *parent, Callback2 call)
    : QWidget(parent)
    , _name_action(name_action)
    , _call(call)
{
    _horisontal_list_panel = new QHBoxLayout(this);

    _label = new QLabel(_name_action);
    _start = new QLineEdit();
    _end   = new QLineEdit();
    _button = new QPushButton("Push");

    connect(_button, SIGNAL(clicked()), this, SLOT(modifyAction()));

    _horisontal_list_panel->addWidget(_label);
    _horisontal_list_panel->addWidget(_start);
    _horisontal_list_panel->addWidget(_end);
    _horisontal_list_panel->addWidget(_button);
}

EdgeChangeSubpanel::~EdgeChangeSubpanel()
{}

void EdgeChangeSubpanel::modifyAction()
{
    qDebug() << _call(_start->text().toStdString(), _end->text().toStdString());
}

